package fr.cnam.foad.nfa035.badges.service;

import fr.cnam.foad.nfa035.badges.service.impl.BadgesWalletRestServiceImpl;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@OpenAPIDefinition(
        info = @Info(
                title = "Service d'accès au Portefeuille de Badge",
                version = "1.0.0-SNAPSHOT",
                description = "API permettant la manipulation d'entité de type DigitalBadges au sein de notre portefeuille au format JSON (Wallet). " +
                        "<br/>Il s'agit simplement, en termes d'opérations, de réponde aux exigences CRUD (Create, Read, Update, et Delete), ",
                license = @License(name = "Tous droits réservés", url = "https://lecnam.net/"),
                contact = @Contact(name = "<Votre Nom>", email = "<votre mail>>")
        )
)
@Configuration
@SpringBootApplication
@ComponentScan("fr.cnam.foad.nfa035.badges")
@EnableAutoConfiguration
public class BadgesServiceApplication {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(BadgesServiceApplication.class, args);
    }
}
