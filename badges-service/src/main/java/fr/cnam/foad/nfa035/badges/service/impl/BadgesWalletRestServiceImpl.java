package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesServiceDAOFactory;
import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Set;

public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {

    @Qualifier("RestServiceSelected")
    @Autowired
    private BadgesServiceDAOFactory badgesServiceDAOFactory ;
    @Autowired
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() throws IOException {
        DirectAccessBadgeWalletDAO jsonBadgeDao = badgesServiceDAOFactory.createInstance("jsonBadge");
        return ResponseEntity
                .ok()
                .body(jsonBadgeDao.getWalletMetadata());
    }
}
